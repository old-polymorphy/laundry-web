'use strict';

const Model = use('Model');

class Record extends Model {
  /**
   * Returns the user.
   * @returns {Object}
   */
  user() {
    return this.belongsTo('App/Models/User');
  }

  /**
   * Returns the time.
   * @returns {Object}
   */
  time() {
    return this.belongsTo('App/Models/Time');
  }

  /**
   * Returns the washer.
   * @returns {Object}
   */
  washer() {
    return this.belongsTo('App/Models/Washer');
  }
}

module.exports = Record;
