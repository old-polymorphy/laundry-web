const Antl = use('Antl');

module.exports.getLocale = function (request) {
  return request.plainCookie('locale') || Antl.currentLocale();
};
