const { validateAll } = use('Validator');
const Helpers = use('Helpers');
const CustomHelpers = use('App/Helpers');
const Antl = use('Antl');

// Models
const User = use('App/Models/User');

// Packages
const uuidv4 = require('uuid/v4');

module.exports = class RegisterForm {
  /**
   * RegisterForm constructor.
   *
   * @param {Object} request
   * @param {Object} response
   *
   * @returns {void}
   */
  constructor(request, response) {
    this._request = request;
    this._response = response;

    this._rules = {
      name: 'alpha|required',
      surname: 'alpha|required',
      email: 'email|required|unique:users',
      phone: 'required|unique:users',
      password: 'required',
      room: 'number|required'
    };

    this._errors = [];
  }

  /**
   * Initiates all validations.
   *
   * @returns {Promise<void>}
   *
   * @private
   */
  async _validate() {
    await this._validateFields();
    await this._validateCard();
  }

  /**
   * Validates fields.
   *
   * @returns {Promise<void>}
   *
   * @private
   */
  async _validateFields() {
    const validation = await validateAll(this._request.all(), this._rules);

    if (validation.fails()) {
      this._errors = validation.messages();
    }
  }

  /**
   * Checks if the file is uploaded.
   *
   * @returns {Promise<void>}
   *
   * @private
   */
  async _validateCard() {
    this.card = this._request.file('card', {
      types: ['image']
    });

    if (!this.card) {
      this._errors.push({
        message: 'required validation failed on card',
        field: 'card',
        validation: 'required'
      });
    } else {
      await this._validateCardType();
    }
  }

  /**
   * Checks for the card's type.
   *
   * @returns {Promise<void>}
   *
   * @private
   */
  async _validateCardType() {
    this.cardName = uuidv4();

    await this.card.move(Helpers.publicPath('assets/images/uploads'), {
      name: `${this.cardName}.${this.card.extname}`,
      overwrite: true
    });

    if (!this.card.moved()) {
      this._errors.push({
        message: this.card.error().message,
        field: this.card.error().fieldName,
        validation: this.card.error().type
      });
    }
  }

  /**
   * Instantiates User class and populates it with the user input.
   *
   * @returns {*}
   *
   * @private
   */
  _createUser() {
    let user = new User();
    let request = this._request;

    user.name = request.body.name;
    user.surname = request.body.surname;
    user.email = request.body.email;
    user.phone = request.body.phone;
    user.password = request.body.password;
    user.room = request.body.room;
    user.card = `${this.cardName}.${this.card.extname}`;

    return user;
  }

  /**
   * Forms a runtime error.
   *
   * @param {Object} error
   *
   * @returns {*|void}
   *
   * @private
   */
  _formRuntimeError(error) {
    let errorText = Antl.forLocale(
      CustomHelpers.getLocale(this._request)
    ).formatMessage('errors.' + error.code);

    let errorStatus = error.status;

    if (!error.status) {
      errorStatus = 500;
    }

    return this._response.status(errorStatus).send(errorText);
  }

  /**
   * Submits the form.
   *
   * @returns {Promise<*|void>}
   */
  async persist() {
    await this._validate();

    // Check for validation errors
    if (this._errors.length) {
      return this._response.status(400).send(this._errors);
    }

    // Trying to save the user
    try {
      await this._createUser().save();
    } catch (error) {
      return this._formRuntimeError(error);
    }

    return this._response.send(
      Antl.forLocale(CustomHelpers.getLocale(this._request)).formatMessage(
        'messages.registered'
      )
    );
  }
};
