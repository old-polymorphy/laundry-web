'use strict';

// Providers
const Env = use('Env');

class HomeController {
  /**
   * Renders the main view.
   *
   * @param view
   * @param request
   * @param response
   *
   * @return {*}
   */
  index({view, request, response}) {
    const environment = Env.get('NODE_ENV');

    return view.render('layout', {environment});
  }
}

module.exports = HomeController;
