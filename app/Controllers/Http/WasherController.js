'use strict';

const Database = use('Database');

class WasherController {
  /**
   * Returns washers from database.
   *
   * @returns {Promise<void>}
   */
  async washers() {
    return await Database.table('washers');
  }
}

module.exports = WasherController;
