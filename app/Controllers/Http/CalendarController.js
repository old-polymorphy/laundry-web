'use strict';

const Database = use('Database');

class CalendarController {
  /**
   * Returns times from database.
   *
   * @returns {Promise<void>}
   */
  async times() {
    return await Database.table('times');
  }
}

module.exports = CalendarController;
