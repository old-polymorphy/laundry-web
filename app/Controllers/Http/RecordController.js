'use strict';

// Models
const Record = use('App/Models/Record');

// Libs
const Pusher = require('pusher');

// Pusher configuration
const pusher = new Pusher({
  appId: '635474',
  key: 'cdc5009cef721bc6792b',
  secret: '7a6417d9d8ea9212d27c',
  cluster: 'eu'
});

class RecordController {
  /**
   * Returns records from the database.
   *
   * @param params
   *
   * @returns {Promise<Response>}
   */
  async recordsByMonth({ params }) {
    return await Record.query()
      .with('user')
      .with('time')
      .with('washer')
      .whereRaw('CAST(strftime("%m", date) AS INTEGER) = ?', [params.month])
      .fetch();
  }

  /**
   * Returns records from database.
   *
   * @returns {Promise<void>}
   */
  async recordsByDate({ params }) {
    return await Record.query()
      .with('user')
      .with('time')
      .with('washer')
      .whereRaw('CAST(strftime("%m", date) AS INTEGER) = ?', [params.month])
      .whereRaw('CAST(strftime("%d", date) AS INTEGER) = ?', [params.date])
      .fetch();
  }

  /**
   * Deletes the record from the database.
   *
   * @param params
   *
   * @return {Promise<*>}
   */
  async delete({ params }) {
    const record = await Record.find(params.id);

    try {
      let response = await record.delete();
      pusher.trigger('records', 'delete-record', {
        message: params.id
      });
      return response;
    } catch (e) {
      return e;
    }
  }

  /**
   * Creates a record.
   *
   * @param request
   * @param response
   *
   * @returns {Promise<*>}
   */
  async create({ request, response }) {
    let record = new Record();
    const requestBody = request.all();

    record.user_id = requestBody['user_id'];
    record.time_id = requestBody['time_id'];
    record.washer_id = requestBody['washer_id'];
    record.date = requestBody['date'];

    // Trying to save a record
    try {
      await record.save();
      const newRecord = await this.getRecordById({ params: { id: record.id } });
      pusher.trigger('records', 'new-record', {
        message: newRecord
      });
      return response.status(200).send(newRecord);
    } catch (e) {
      return response.status(e.status).send(e.message);
    }
  }

  /**
   * Fetches the particular record by it's id.
   *
   * @param params
   *
   * @returns {Promise<Response>}
   */
  async getRecordById({ params }) {
    return await Record.query()
      .with('user')
      .with('time')
      .with('washer')
      .where('id', params.id)
      .fetch();
  }

  /**
   * Fetches records.
   *
   * @param params
   *
   * @returns {Promise<*>}
   */
  async get({ params }) {
    return await Record.query()
      .where('time_id', params.time)
      .where('washer_id', params.washer)
      .whereRaw('strftime("%d", date) = ?', params.date)
      .whereRaw('strftime("%m", date) = ?', params.month)
      .fetch();
  }
}

module.exports = RecordController;
