'use strict';

// Forms
const RegisterForm = use('App/Forms/Register');

// Adonis
const Antl = use('Antl');

// Helpers
const CustomHelpers = use('App/Helpers');

class UserController {
  /**
   * Logs in the user.
   *
   * @param request
   * @param response
   * @param auth
   *
   * @return {Promise<*|void>}
   */
  async login({ request, response, auth }) {
    let user = request.all();

    try {
      await auth.attempt(user.email, user.password);
    } catch (error) {
      return response.status(error.status).send(error);
    }

    return response.send(auth.user);
  }

  /**
   * Registers a user.
   *
   * @param auth
   * @param request
   * @param response
   *
   * @return {Promise<*|void>}
   */
  async register({ auth, request, response }) {
    const registerForm = new RegisterForm(request, response);

    return registerForm.persist();
  }

  /**
   * Terminates the session.
   *
   * @param response
   * @param auth
   *
   * @return {Promise<*|void>}
   */
  async exit({ response, auth }) {
    try {
      return response.send(await auth.logout());
    } catch (error) {
      return response.send(error.name);
    }
  }

  /**
   * Return the user information.
   *
   * @param response
   * @param auth
   *
   * @return {Promise<*|void>}
   */
  async me({ response, auth }) {
    try {
      await auth.check();
      return response.status(200).send(auth.user);
    } catch {
      return response.status(200).send(null);
    }
  }
}

module.exports = UserController;
