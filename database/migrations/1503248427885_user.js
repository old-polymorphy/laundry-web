'use strict';

const Schema = use('Schema');

class UserSchema extends Schema {
  up() {
    this.create('users', table => {
      table.increments();
      table.string('name', 80).notNullable();
      table.string('surname', 80).notNullable();
      table.int('room', 4).notNullable();
      table
        .string('email', 254)
        .notNullable()
        .unique();
      table.string('password', 60).notNullable();
      table
        .string('phone', 32)
        .notNullable()
        .unique();
      table.string('card').unique();
      table
        .boolean('active', 1)
        .notNullable()
        .default(false);
      table.timestamps();
    });
  }

  down() {
    this.dropIfExists('users');
  }
}

module.exports = UserSchema;
