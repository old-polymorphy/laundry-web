'use strict';

const Schema = use('Schema');

class RecordSchema extends Schema {
  up() {
    this.create('records', (table) => {
      table.increments().primary();
      table.integer('user_id').unsigned().references('id').inTable('users');
      table.integer('time_id').unsigned().references('id').inTable('times');
      table.integer('washer_id').unsigned().references('id').inTable('washers');
      table.date('date').notNullable();
      table.unique(['time_id', 'washer_id', 'date']);
      table.timestamps();
    })
  }

  down() {
    this.drop('records')
  }
}

module.exports = RecordSchema;
