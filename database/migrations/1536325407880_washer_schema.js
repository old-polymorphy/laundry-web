'use strict';

const Schema = use('Schema');

class WasherSchema extends Schema {
  up() {
    this.create('washers', (table) => {
      table.increments();
      table.string('name');
      table.timestamps();
    })
  }

  down() {
    this.drop('washers')
  }
}

module.exports = WasherSchema;
