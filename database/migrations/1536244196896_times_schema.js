'use strict';

const Schema = use('Schema');

class TimesSchema extends Schema {
  up() {
    this.create('times', (table) => {
      table.increments();
      table.time('start').notNullable().unique();
      table.time('end').notNullable().unique();
      table.timestamps();
    })
  }

  down() {
    this.dropIfExists('times');
  }
}

module.exports = TimesSchema;
