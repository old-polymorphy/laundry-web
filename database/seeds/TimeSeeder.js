'use strict';

const Factory = use('Factory');
const moment = require('moment');

class TimeSeeder {
  /**
   * Runs the seeder.
   * @returns {Promise<void>}
   */
  async run() {
    this.getTimes().forEach((time) => {
      Factory
        .model('App/Models/Time')
        .create({time: time});
    });
  }

  /**
   * Gets all times.
   * @returns {Array}
   */
  getTimes() {
    let times = [];
    let hour = 7;

    for (hour; hour < 24; hour++) {
      if (hour === 14) {
        continue;
      }

      times.push({
        start: moment(`${hour}00`, 'hmm').format('HH:mm'),
        end: moment(`${hour + 1}00`, 'hmm').format('HH:mm'),
      });
    }

    return times;
  }
}

module.exports = TimeSeeder;
