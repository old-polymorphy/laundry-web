'use strict';

const Factory = use('Factory');

/**
 * RecordSeeder class.
 */
class RecordSeeder {
  async run() {
    await Factory
      .model('App/Models/Record')
      .create();
  }
}

module.exports = RecordSeeder;
