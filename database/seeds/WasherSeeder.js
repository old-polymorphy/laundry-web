'use strict';

const Factory = use('Factory');

/**
 * WasherSeeder class.
 */
class WasherSeeder {
  async run() {
    await Factory
      .model('App/Models/Washer')
      .createMany(4);
  }
}

module.exports = WasherSeeder;
