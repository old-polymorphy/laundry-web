'use strict';

const Factory = use('Factory');

/**
 * User factory.
 */
Factory.blueprint('App/Models/User', () => {
  return {
    name: 'Tester',
    surname: 'Testerman',
    room: 46,
    email: 'test@test.ru',
    password: 'test',
    phone: '89999999999',
    card: undefined,
    active: false
  };
});

/**
 * Time factory.
 */
Factory.blueprint('App/Models/Time', (faker, index, data) => {
  return {
    start: data.time.start,
    end: data.time.end
  };
});

/**
 * Washer factory.
 */
Factory.blueprint('App/Models/Washer', faker => {
  return {
    name: faker.name()
  };
});

/**
 * Record factory.
 */
Factory.blueprint('App/Models/Record', faker => {
  return {
    user_id: 1,
    time_id: 1,
    washer_id: 1,
    date: '2018-09-24'
  };
});
