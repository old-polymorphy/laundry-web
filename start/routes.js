'use strict';

const Route = use('Route');

// Pages
Route.get('/', 'HomeController.index');

// API
Route.get('/me', 'UserController.me');
Route.get('/times', 'CalendarController.times');
Route.get('/washers', 'WasherController.washers');

// Authentication
Route.post('/register', 'UserController.register');
Route.post('/login', 'UserController.login');
Route.get('/exit', 'UserController.exit');

// Records CRUD
Route.get('/records/:month', 'RecordController.recordsByMonth');
Route.get('/records/:month/:date', 'RecordController.recordsByDate');
Route.get('/records/:time/:washer/:date/:month', 'RecordController.get');
Route.post('/records', 'RecordController.create').middleware(['auth']);
Route.delete('/records/:id', 'RecordController.delete').middleware(['auth']);
