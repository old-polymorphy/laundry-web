// Store
import store from '../store/index';

// Vue
import Vue from 'vue';
import Component from 'vue-class-component';

@Component
export default class AvailabilityMixin extends Vue {
  /**
   * Check if there are more than 4 records.
   *
   * @param {Object[]} records
   *
   * @return boolean
   */
  checkMonthLimit(records) {
    let id = store.state.user.data.id;

    records = records.filter(record => {
      return parseInt(record.user_id) === id;
    });

    return records.length > 3;
  }
}
