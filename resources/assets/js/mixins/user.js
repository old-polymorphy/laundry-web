// Store
import store from '../store/index';

// Vue
import Vue from 'vue';
import Component from 'vue-class-component';

@Component
export default class UserMixin extends Vue {
  /**
   * DateMixin constructor.
   * 
   * @constructs DateMixin
   */
  constructor() {
    super();

    this.user = store.getters.getUser
  }
}
