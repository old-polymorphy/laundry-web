// Store
import store from '../store/index';

// Vue
import Vue from 'vue';
import Component from 'vue-class-component';

@Component
export default class RecordsMixin extends Vue {
  /**
   * Returns current month records.
   *
   * @returns {Object[]}
   */
  get records() {
    return store.getters.getRecords;
  }
}
