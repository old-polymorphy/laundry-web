// Store
import store from '../store/index';

// Libs
import Cookies from 'js-cookie';

// Vue
import Vue from 'vue';
import Component from 'vue-class-component';

const locales = {
  en: {
    errors: require('../../../locales/en/errors'),
    interface: require('../../../locales/en/interface'),
    messages: require('../../../locales/en/messages'),
    calendar: require('../../../locales/en/calendar'),
    rules: require('../../../locales/en/rules'),
    validation: require('../../../locales/en/validation')
  },
  ru: {
    errors: require('../../../locales/ru/errors'),
    interface: require('../../../locales/ru/interface'),
    messages: require('../../../locales/ru/messages'),
    calendar: require('../../../locales/ru/calendar'),
    rules: require('../../../locales/ru/rules'),
    validation: require('../../../locales/ru/validation')
  },
  no: {
    errors: require('../../../locales/no/errors'),
    interface: require('../../../locales/no/interface'),
    messages: require('../../../locales/no/messages'),
    calendar: require('../../../locales/no/calendar'),
    rules: require('../../../locales/no/rules'),
    validation: require('../../../locales/no/validation')
  }
};

@Component
export default class LocalesMixin extends Vue {
  /**
   * Returns the object with translations.
   *
   * @returns {Object}
   */
  get locale() {
    return locales[store.state.locale.data];
  }

  /**
   * Returns the current locale from Vuex.
   *
   * @returns {string}
   */
  getCurrentLocale() {
    return store.state.locale.data;
  }

  /**
   * Handles locale change.
   *
   * @param {any} e — Event object.
   *
   * @returns {void}
   */
  handleLocaleChange(e) {
    Cookies.set('locale', e.target.value);
    store.commit('SET_LOCALE', e.target.value);
  }
}
