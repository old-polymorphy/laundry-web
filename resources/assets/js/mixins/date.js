// Store
import store from '../store/index';

// Vue
import Vue from 'vue';
import Component from 'vue-class-component';

@Component
export default class DateMixin extends Vue {
  /**
   * DateMixin constructor.
   * 
   * @constructs DateMixin
   */
  constructor() {
    super();

    this.times = store.getters.getTimes;
    this.washers = store.getters.getWashers;
  }
}
