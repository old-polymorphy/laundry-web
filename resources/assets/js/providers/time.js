// Libs
import axios from 'axios';

/**
 * User service.
 */
export default {
  /**
   * Gets all times.
   *
   * @return {Promise<*>}
   */
  async all() {
    let times = await axios.get('/times');

    return times.data;
  },

  /**
   * Gets all times.
   *
   * @return {Promise<*>}
   */
  async washers() {
    let washers = await axios.get('/washers');

    return washers.data;
  },

  /**
   * Gets all records.
   *
   * @param {number} month
   *
   * @return {Promise<*>}
   */
  async recordsByMonth(month) {
    let records = await axios.get(`/records/${month}`);

    return records.data;
  },

  /**
   * Deletes the record from the database.
   *
   * @param {number} id
   *
   * @return {AxiosPromise}
   */
  deleteRecord(id) {
    return axios.delete(`/records/${id}`);
  },

  /**
   * Creates a record.
   *
   * @param {Object} data
   *
   * @return {AxiosPromise<any>}
   */
  createRecord(data) {
    return axios.post(`/records/`, data);
  }
};
