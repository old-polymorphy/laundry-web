// Libs
import axios from 'axios';

// Store
import store from '../store/index';

/**
 * User service.
 */
export default {
  /**
   * Returns authenticated user.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  async me() {
    try {
      const response = await axios.get('/me');
      return response.data;
    } catch (error) {
      console.error(error);
    }
  },

  /***
   * Checks whether the current user state is empty.
   *
   * @returns {boolean}
   */
  check() {
    return Boolean(Object.keys(store.getters.getUser).length);
  },

  /**
   * Logs in the user.
   *
   * @param {Object} user User credentials.
   *
   * @returns {AxiosPromise<any>}
   */
  login(user) {
    return axios.post('/login', user);
  },

  /**
   * Terminates the session.
   *
   * @returns {AxiosPromise<any>}
   */
  exit() {
    return axios.get('/exit');
  },

  /**
   *
   * Registers a user.
   *
   * @param user
   *
   * @returns {AxiosPromise<any>}
   */
  register(user) {
    return axios.post('/register', user, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  }
};
