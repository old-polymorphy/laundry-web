// Vue
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';

// Components
import GuestPage from './components/pages/guest-page/guest-page.vue';
import ProfilePage from './components/pages/profile-page/profile-page.vue';
import RegisterPage from './components/pages/register-page/register-page.vue';
import LoginPage from './components/pages/login-page/login-page.vue';
import CalendarPage from './components/pages/calendar-page/calendar-page.vue';
import DatePage from './components/pages/date-page/date-page.vue';
import RecordBlock from './components/ui/record-block/record-block.vue';

// Libs
import axios from 'axios';
import Cookies from 'js-cookie';
import Pusher from 'pusher-js';

// Code
import '../sass/style.sass';

// Store
import store from './store/index';

// Providers
import userProvider from './providers/user';

// TODO: MOVE
// Polyfills
String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

// TODO: REFACTORING
const pusher = new Pusher('cdc5009cef721bc6792b', {
  cluster: 'eu',
  forceTLS: true
});

const channel = pusher.subscribe('records');

channel.bind('new-record', data => {
  store.commit('ADD_RECORD', data.message[0]);
});

channel.bind('delete-record', data => {
  store.commit('FILTER_RECORDS', data.message);
});

// TODO: SIMPLIFY GUARDS
/**
 * Auth middleware.
 *
 * @param to
 * @param from
 * @param next
 *
 * @returns {void}
 */
const authGuard = (to, from, next) => {
  if (!userProvider.check()) {
    router.push('/');
  } else {
    next();
  }
};

/**
 * Unauth middleware.
 *
 * @param to
 * @param from
 * @param next
 *
 * @returns {void}
 */
const unauthGuard = (to, from, next) => {
  if (userProvider.check()) {
    router.push('/profile');
  } else {
    next();
  }
};

/**
 * Routes for Vue Router
 *
 * @constant routes
 */
const routes = [
  {
    path: '/',
    component: GuestPage,
    beforeEnter: unauthGuard
  },
  {
    path: '/login',
    component: LoginPage,
    beforeEnter: unauthGuard
  },
  {
    path: '/register',
    component: RegisterPage,
    beforeEnter: unauthGuard
  },
  {
    path: '/profile',
    component: ProfilePage,
    beforeEnter: authGuard
  },
  {
    path: '/calendar',
    component: CalendarPage,
    beforeEnter: authGuard
  },
  {
    path: '/day/:number',
    component: DatePage,
    beforeEnter: authGuard
  }
];

const router = new VueRouter({
  base: window.location.pathName,
  routes
});

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

/**
 * Initiates fetching of all data.
 *
 * @returns {Promise<any[]>}
 */
function fetchData() {
  return Promise.all([
    store.dispatch('fetchUser'),
    store.dispatch('fetchRecords'),
    store.dispatch('fetchWashers'),
    store.dispatch('fetchTimes')
  ]);
}

const csrfToken = document
  .querySelector('meta[name="csrf-token"]')
  .getAttribute('content');

fetchData()
  .then(() => {
    axios.defaults.headers.common['X-CSRF-Token'] = csrfToken;

    if (Cookies.get('locale')) {
      store.commit('SET_LOCALE', Cookies.get('locale'));
    }

    new Vue({
      el: '#app',
      router,
    });
  })
  .catch((error) => {
    console.error(error.message);
  });
