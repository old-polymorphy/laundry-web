// Services
import timeProvider from '../../../providers/time';

// Mixins
import AvailabilityMixin from '../../../mixins/availability';
import LocalesMixin from '../../../mixins/locales';

// Store
import store from '../../../store';

// Vue
import Vue from 'vue';
import Component, { mixins } from 'vue-class-component';

@Component({
  props: {
    time: {
      type: Object,
      required: true,
    },
    washer: {
      type: Object,
      required: true,
    },
    month: {
      type: Number,
      required: true,
    },
    records: {
      type: Array,
      required: true,
    },
    getRecord: {
      type: Function,
      required: true,
    },
    openRecordMenu: {
      type: Function,
      required: true,
    },
    handleOpenAddForm: {
      type: Function,
      required: true,
    }
  },
})
export default class RecordBlock extends mixins(AvailabilityMixin, LocalesMixin) {
  /**
   * Checks whether the record-block is created by the current user.
   *
   * @return {boolean}
   */
  checkAuthor() {
    return this.record.user_id === store.state.user.data.id;
  }

  /**
   * Returns record-block text.
   *
   * @return {string}
   */
  getRecordText() {
    return `${this.record.user.surname} ${this.record.user.name}, ${
      this.record.user.room
      }`;
  }

  /**
   * Build a string to show in a menu.
   *
   * @return {string}
   */
  getMenuDate() {
    return `${this.time.start}–${this.time.end}`;
  }

  /**
   * Returns mobile record-block text.
   *
   * @return {string}
   */
  getMobileRecordText() {
    return `${this.record.user.surname}, ${this.record.user.room}`;
  }

  /**
   * Deletes the record-block.
   *
   * @param {Object} e Event.
   *
   * @return {void}
   */
  deleteRecord(e) {
    e.preventDefault();
    e.stopPropagation();

    if (this.checkAuthor()) {
      timeProvider.deleteRecord(this.record.id).catch(error => {
        console.error(error);
      });
    }
  }

  /**
   * Returns the current record-block.
   *
   * @return {Object}
   */
  get record() {
    let result;

    this.records.forEach(record => {
      if (
        parseInt(record.washer_id) === this.washer.id &&
        parseInt(record.time_id) === this.time.id
      ) {
        Vue.set(record, 'menu', false);

        result = record;
      }
    });

    return result;
  }

  /**
   * Checks if the time is not the third in the row.
   *
   * @return {boolean}
   */
  get checkRowLimit() {
    let id = store.state.user.data.id;
    let records = [];

    // Fill the array with two previous and two next blocks
    [1, 2, -1, -2].forEach(offset => {
      records.push(this.getRecord(this.time.id + offset, this.washer.id));
    });

    // Check if the previous ones exist and are created by the current user
    let previous = [records[0], records[1]].every(record => {
      return record && record.user_id === id;
    });

    // Check if the next ones exist and are created by the current user
    let next = [records[2], records[3]].every(record => {
      return record && record.user_id === id;
    });

    return previous || next;
  }

  /**
   * Checks if the time is available.
   *
   * @return {boolean}
   */
  get isAvailable() {
    const rowLimit = this.checkRowLimit;
    const monthLimit = this.checkMonthLimit(store.getters.getRecords);

    return !rowLimit && !monthLimit;
  }
};
