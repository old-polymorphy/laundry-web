// Libs
import moment from 'moment';

// Store
import store from '../../../store';

// Providers
import timeProvider from '../../../providers/time';

// Mixins
import LocalesMixin from '../../../mixins/locales';

// Vue
import Component, { mixins } from 'vue-class-component';

@Component({
  props: {
    date: {
      type: Object,
      required: true,
    },
    handleCloseAddForm: {
      type: Function,
      required: true,
    },
    selectedTime: {
      type: Object,
      required: true,
    },
    selectedWasher: {
      type: Object,
      required: true,
    },
    times: {
      type: Array,
      required: true,
    },
    washers: {
      type: Array,
      required: true,
    }
  },
})
export default class AddRecordForm extends mixins(LocalesMixin) {
  /**
   * AddRecordForm constructor.
   * 
   * @constructs AddRecordForm
   */
  constructor() {
    super();

    this.loading = false;
  }

  /**
   * Returns a string containing a time range.
   *
   * @param {Object} time
   *
   * @returns {string}
   */
  getTimeRange(time) {
    const date = moment(this.date)
      .locale(this.getCurrentLocale())
      .format('dddd, Do MMMM YYYY');

    return `${date}, ${time.start}–${time.end}`;
  }

  /**
   * Returns a string with the current user's name.
   *
   * @returns {string}
   */
  getCurrentUserName() {
    const user = store.state.user.data;

    return `${user.name} ${user.surname}`;
  }

  /**
   * Return the current user data.
   *
   * @returns {Object}
   */
  getCurrentUser() {
    return store.state.user.data;
  }

  /**
   * Instantiates a FormData and populates it with the data.
   *
   * @returns {FormData}
   */
  assembleFormData() {
    let formData = new FormData();

    formData.append('user_id', store.state.user.data.id);
    formData.append('time_id', this.selectedTime.id);
    formData.append('washer_id', this.selectedWasher.id);
    formData.append('date', moment(this.date).format('YYYY-MM-DD'));

    return formData;
  }

  /**
   * Handles the form.
   *
   * @returns {void}
   */
  handleAddForm() {
    this.loading = true;

    timeProvider.createRecord(this.assembleFormData())
      .then(this.handleCloseAddForm)
      .catch((err) => console.error(err.message))
      .finally(() => this.loading = false);
  }
}
