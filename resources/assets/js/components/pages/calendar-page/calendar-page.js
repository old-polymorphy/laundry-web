// Libs
import calendar from 'calendar-js';
import Cookies from 'js-cookie';
import moment from 'moment';

// Mixins
import AvailabilityMixin from '../../../mixins/availability';
import LocalesMixin from '../../../mixins/locales';
import RecordsMixin from '../../../mixins/records';

// Store
import store from '../../../store';

// Vue
import Component, { mixins } from 'vue-class-component';

@Component
export default class CalendarPage extends mixins(AvailabilityMixin, LocalesMixin, RecordsMixin) {
  /**
   * CalendarPage constructor.
   * 
   * @constructs CalendarPage
   */
  constructor() {
    super();

    this.calendar = calendar().of(moment().year(), moment().month());
    this.accepted = false
  }
  /**
   * Saves acceptance to cookie.
   *
   * @returns {void}
   */
  accept() {
    Cookies.set('accepted', true);

    this.accepted = true;
  }

  /**
   * Opens the day.
   *
   * @param {number} day
   *
   * @returns {void}
   */
  openDay(day) {
    if (this.checkDay(day)) {
      this.$router.push(`/day/${day}`);
    }
  }

  /**
   * Checks if the date is in the past.
   *
   * @param {number} day
   *
   * @returns {boolean}
   */
  checkDay(day) {
    const selectedDay = moment().set({ date: day });
    const currentDay = moment().set({ date: moment().date() });

    return selectedDay.diff(currentDay) >= 0;
  }

  /**
   * Checks if the day contains any records of the current user.
   *
   * @param {number} day
   *
   * @returns {boolean}
   */
  checkIfMine(day) {
    let counter = 0;

    for (let i = 0; i < this.records.length; i++) {
      if (
        this.records[i].user_id === store.state.user.data.id &&
        moment(this.records[i].date).date() === day
      ) {
        counter++;
      }
    }

    return Boolean(counter);
  }

  /**
   * Checks if there are available times on the day.
   *
   * @param {number} day
   *
   * @returns {boolean}
   */
  checkIfHasAvailable(day) {
    let counter = 0;

    for (let i = 0; i < this.records.length; i++) {
      if (moment(this.records[i].date).date() === day) {
        counter++;
      }
    }

    return Boolean(counter < 64) && !this.checkMonthLimit(this.records);
  }

  /**
   * Vue hook.
   */
  created() {
    this.accepted = Boolean(Cookies.get('accepted'));
  }
};
