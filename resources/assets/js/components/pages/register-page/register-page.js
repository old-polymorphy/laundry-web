// Providers
import userProvider from '../../../providers/user';

// Mixins
import LocalesMixin from '../../../mixins/locales';

// Vue
import Component, { mixins } from 'vue-class-component';

@Component
export default class RegisterPage extends mixins(LocalesMixin) {
  /**
   * RegisterPage constructor.
   *
   * @constructs RegisterPage
   */
  constructor() {
    super();

    this.name = '';
    this.surname = '';
    this.email = '';
    this.phone = '';
    this.password = '';
    this.room = '';
    this.file = '';
    this.errors = [];
    this.runtime = '';
    this.loading = false;
  }

  /**
   * Creates an object, populates it with user
   * input and submits the form.
   *
   * @return {void}
   */
  submit() {
    const user = new FormData();

    user.append('name', this.name);
    user.append('surname', this.surname);
    user.append('email', this.email);
    user.append('phone', this.phone);
    user.append('password', this.password);
    user.append('room', this.room);
    user.append('card', this.file);

    this.register(user);
  }

  /**
   * Resets all fields and errors.
   *
   * @return {void}
   */
  reset() {
    this.name = '';
    this.surname = '';
    this.email = '';
    this.phone = '';
    this.password = '';
    this.room = '';
    this.file = '';
    this.errors = [];
  }

  /**
   * Registers the user.
   *
   * @param {Object} user — User credentials.
   *
   * @return {void}
   */
  register(user) {
    this.errors = [];
    this.loading = true;

    userProvider.register(user)
      .then(() => this.$router.push('/login'))
      .catch((err) => {
        if (err.response.status === 400) {
          this.errors = err.response.data;

          document
            .querySelector('.is-danger')
            .classList.add('animated', 'shake');
        } else {
          alert(`${this.locale.errors.runtime} (${err.response.data})`);
        }
      })
      .then(() => this.loading = false);
  }

  /**
   * Handles file uploading.
   *
   * @returns {void}
   */
  handleFileUpload(event) {
    this.file = event.target.files[0];
  }
};
