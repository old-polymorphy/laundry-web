// Store
import store from '../../../store';

// Providers
import userProvider from '../../../providers/user';

// Mixins
import LocalesMixin from '../../../mixins/locales';
import UserMixin from '../../../mixins/user';

// Vue
import Component, { mixins } from 'vue-class-component';

@Component
export default class ProfilePage extends mixins(LocalesMixin, UserMixin) {
  /**
   * ProfilePage constructor.
   *
   * @constructs ProfilePage
   */
  constructor() {
    super();

    this.loading = false;
  }

  /**
   * Terminates the session.
   *
   * @returns {void}
   */
  exit() {
    this.loading = true;

    userProvider.exit()
      .then(() => {
        store.commit('UNSET_USER', null);
        this.$router.push('/');
      })
      .catch((err) => console.error(err.message))
      .finally(() => this.loading = false);
  }

  /**
   * Returns the greeting string.
   *
   * @returns {string}
   */
  greet() {
    return `${this.locale.messages.greeting}, ${store.state.user.data.name}!`;
  }
};
