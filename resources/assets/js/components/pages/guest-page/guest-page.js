// Mixins
import LocalesMixin from '../../../mixins/locales';

// Vue
import Component, { mixins } from 'vue-class-component';

@Component
export default class GuestPage extends mixins(LocalesMixin) { };
