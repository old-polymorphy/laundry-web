// Store
import store from '../../../store';

// Services
import userProvider from '../../../providers/user';

// Mixins
import LocalesMixin from '../../../mixins/locales';

// Vue
import Component, { mixins } from 'vue-class-component';

@Component
export default class LoginPage extends mixins(LocalesMixin) {
  /**
   * LoginPage constructor.
   * 
   * @constructs LoginPage
   */
  constructor() {
    super();

    this.email = '';
    this.password = '';
    this.error = '';
    this.loading = false;
  }

  /**
   * Handles the submitting.
   * 
   * @returns {void}
   */
  submit() {
    const user = {
      email: this.email,
      password: this.password
    };

    this.login(user);
  }

  /**
   * Resets the fields.
   * 
   * @returns {void}
   */
  reset() {
    this.email = '';
    this.password = '';
    this.error = '';
  }

  /**
   * Logs in the user.
   *
   * @param {Object} user — User credentials.
   *
   * @returns {void}
   */
  login(user) {
    this.error = '';
    this.loading = true;

    userProvider.login(user)
      .then((response) => {
        store.commit('SET_USER', response.data);
        this.$router.push('/profile');
      })
      .catch((err) => {
        const response = err.response;
        const errorDict = this.locale.errors;
        const errorMessage = errorDict[response.status] || response.statusText;

        this.error = errorDict.error + ': ' + errorMessage;

        document.querySelector('.is-danger').classList.add('animated', 'shake');
      })
      .finally(() => this.loading = false);
  }
};
