// Libs
import moment from 'moment';

// Vue
import AddRecordForm from '../../ui/add-record-form/add-record-form.vue';
import RecordBlock from '../../ui/record-block/record-block.vue';

// Mixins
import DateMixin from '../../../mixins/date';
import LocalesMixin from '../../../mixins/locales';

// Store
import store from '../../../store';

// Vue
import Component, { mixins } from 'vue-class-component';

@Component({
  components: {
    AddRecordForm,
    RecordBlock,
  },
})
export default class DatePage extends mixins(DateMixin, LocalesMixin) {
  /**
   * DatePage constructor.
   * 
   * @constructs DatePage
   */
  constructor() {
    super();

    this.currentWasher = 0;

    // Add form
    this.addForm = false;
    this.selectedTime = null;
    this.selectedWasher = null;
  }

  /**
   * Returns a record.
   *
   * @param {number} timeId   ID of the time.
   * @param {number} washerId ID of the washer.
   *
   * @returns {Object}
   */
  getRecord(timeId, washerId) {
    let result;

    this.records.forEach(record => {
      if (record.washer_id === washerId && record.time_id === timeId) {
        result = record;
      }
    });

    return result;
  }

  /**
   * Opens the record's menu.
   *
   * @param {Object} record Object with the record's data.
   *
   * @returns {void}
   */
  openRecordMenu(record) {
    if (record && !record.menu) {
      this.records.forEach(record => {
        record.menu = false;
      });
    }

    // Toggle a menu if there's a record.
    if (record) {
      record.menu = !Boolean(record.menu);
    }
  }

  /**
   * Opens the record form.
   *
   * @returns {void}
   */
  handleOpenAddForm(time, washer) {
    this.addForm = true;

    this.selectedTime = Object.assign({}, time);
    this.selectedWasher = Object.assign({}, washer);
  }

  /**
   * Closes the record form.
   *
   * @returns {void}
   */
  handleCloseAddForm() {
    this.addForm = false;

    this.selectedTime = null;
    this.selectedWasher = null;
  }

  /**
   * Handles a click on "previous" arrow.
   *
   * @returns {void}
   */
  handlePreviousWasher() {
    if (this.currentWasher) {
      this.currentWasher--;
    }
  }

  /**
   * Handles click on "next" arrow.
   *
   * @returns {void}
   */
  handleNextWasher() {
    if (this.currentWasher < 3) {
      this.currentWasher++;
    }
  }

  /**
   * Vue hook.
   */
  created() {
    this.date = moment().set({ date: this.$route.params.number });
  }

  /**
   * Returns the records.
   *
   * @return Array<Object>
   */
  get records() {
    return store.getters.getDateRecords(this.date.date());
  }
};
