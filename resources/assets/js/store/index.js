import Vue from 'vue';
// Vue
import Vuex from 'vuex';

// Modules
import locale from './modules/locale';
import records from './modules/records';
import user from './modules/user';

Vue.use(Vuex);
Vue.config.devtools = true;

export default new Vuex.Store({
  modules: {
    locale,
    records,
    user
  }
});
