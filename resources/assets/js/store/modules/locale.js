const state = {
  data: 'en'
};

const mutations = {
  SET_LOCALE(state, payload) {
    state.data = payload;
  }
};

export default {
  state,
  mutations
};
