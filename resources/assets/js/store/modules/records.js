import timeProvider from '../../providers/time';
import moment from 'moment';

const state = {
  records: [],
  washers: [],
  times: []
};

const getters = {
  /**
   * Returns all records
   *
   * @param state Current state.
   *
   * @returns {User.records|Array|Object[]|*|recordsMixin.computed.records|{actions, getters, mutations, state}}
   */
  getRecords(state) {
    return state.records;
  },

  /**
   * Returns all records.
   *
   * @param state Current state.
   *
   * @returns {User.records|Array|Object[]|*|recordsMixin.computed.records|{actions, getters, mutations, state}}
   */
  getDateRecords(state) {
    return date => {
      return state.records.filter(record => {
        return moment(record.date).date() === date;
      });
    };
  },

  /**
   * Returns all times.
   *
   * @param state Current state.
   *
   * @returns {CalendarController.times|Array|*|times|{user, nice, sys, idle, irq}|dateMixin.computed.times}
   */
  getTimes(state) {
    return state.times;
  },

  /**
   * Returns all washers.
   *
   * @param state Current state.
   *
   * @returns {default.washers|WasherController.washers|Array|*|dateMixin.computed.washers}
   */
  getWashers(state) {
    return state.washers;
  }
};

const mutations = {
  /**
   * Sets records.
   *
   * @param {Object} state     Current state.
   * @param {Object[]} payload Array of records.
   *
   * @returns {void}
   */
  SET_RECORDS(state, payload) {
    state.records = payload;
  },

  /**
   * Adds a record to the array.
   *
   * @param {Object} state   Current state.
   * @param {Object} payload Record object.
   *
   * @returns {void}
   */
  ADD_RECORD(state, payload) {
    state.records.push(payload);
  },

  /**
   * Removes a record.
   *
   * @param {Object} state   Current state.
   * @param {number} payload ID of the record.
   *
   * @returns {void}
   */
  FILTER_RECORDS(state, payload) {
    state.records = state.records.filter(record => {
      return record.id !== parseInt(payload);
    });
  },

  /**
   * Sets washers.
   *
   * @param {Object} state   Current state.
   * @param {Object} payload Washers fetched.
   *
   * @returns {void}
   */
  SET_WASHERS(state, payload) {
    state.washers = payload;
  },

  /**
   * Sets times.
   *
   * @param {Object} state   Current state.
   * @param {Object} payload Times fetched.
   *
   * @returns {void}
   */
  SET_TIMES(state, payload) {
    state.times = payload;
  }
};

const actions = {
  /**
   * Fetches records from the database.
   *
   * @param commit
   *
   * @returns {Promise<any>}
   */
  async fetchRecords({ commit }) {
    const response = await timeProvider.recordsByMonth(moment().month() + 1);
    commit('SET_RECORDS', response);
  },

  /**
   * Fetches washers from the database.
   *
   * @param commit
   *
   * @returns {Promise<any>}
   */
  async fetchWashers({ commit }) {
    const response = await timeProvider.washers();
    commit('SET_WASHERS', response);
  },

  /**
   * Fetches times from the database.
   *
   * @param commit
   *
   * @returns {Promise<any>}
   */
  async fetchTimes({ commit }) {
    const response = await timeProvider.all();
    commit('SET_TIMES', response);
  }
};

export default {
  actions,
  getters,
  mutations,
  state
};
