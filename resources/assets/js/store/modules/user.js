import userProvider from '../../providers/user';

const state = {
  data: {}
};

const mutations = {
  SET_USER(state, payload) {
    state.data = payload;
  },

  UNSET_USER(state) {
    state.data = {};
  }
};

const getters = {
  /**
   * Returns all records
   *
   * @param state Current state.
   *
   * @returns {User.records|Array|Object[]|*|recordsMixin.computed.records|{actions, getters, mutations, state}}
   */
  getUser(state) {
    return state.data;
  }
};

const actions = {
  /**
   * Fetches the user from the database.
   *
   * @param commit
   *
   * @returns {Promise<any>}
   */
  async fetchUser({ commit }) {
    const response = await userProvider.me();
    commit('SET_USER', response);
  }
};

export default {
  actions,
  getters,
  mutations,
  state
};
